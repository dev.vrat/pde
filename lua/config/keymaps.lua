local shortcut = vim.keymap
local onNormalMode = "n"
local onVisualMode = "v"
local silentAndNoremap = { noremap = true, silent = true }

-- Sourcing and Reloading --
shortcut.set(onNormalMode, '<leader>s', '<cmd>source ~/.config/nvim/init.lua<CR>', {})

-- Project Navigation (eagerly loaded) --
shortcut.set(onNormalMode, "<leader>e", ":NvimTreeToggle<CR>", silentAndNoremap)

-- Telescope (eagerly loaded) --
local telescope_builtin = require('telescope.builtin')
shortcut.set(onNormalMode, '<leader>tf', telescope_builtin.find_files, {})
shortcut.set(onNormalMode, '<leader>tg', telescope_builtin.live_grep, {})
shortcut.set(onNormalMode, '<leader>tb', telescope_builtin.buffers, {})
shortcut.set(onNormalMode, '<leader>th', telescope_builtin.help_tags, {})
shortcut.set(onNormalMode, '<leader>tc', '<cmd>Telescope find_files cwd=~/.config/nvim<CR>', {})

-- Comment (eagerly loaded) --
shortcut.set(onNormalMode, '<C-/>', 'gcc', { noremap = false })
shortcut.set(onVisualMode, '<C-/>', 'gcc', { noremap = false })
shortcut.set(onVisualMode, '<', '<gv')  -- to avoid losing the visual mode on 1st indentation.
shortcut.set(onVisualMode, '>', '>gv')  -- to avoid losing the visual mode on 1st indentation.
