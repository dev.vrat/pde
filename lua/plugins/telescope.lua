local config = function()
  local telescope = require('telescope')
  telescope.setup()
end

return {
  'nvim-telescope/telescope.nvim', tag = '0.1.4',
  dependencies = { 'nvim-lua/plenary.nvim' },
  lazy = false,
  config = config,
}
